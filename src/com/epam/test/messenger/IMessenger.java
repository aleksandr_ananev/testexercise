package com.epam.test.messenger;

import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;

import java.util.List;

public interface IMessenger {
    boolean post(User usr, Message msg) throws UserNotFoundException;

    List<Message> read(User usr) throws UserNotFoundException;

    void follow(User follower, User followed) throws UserNotFoundException;

    List<Message> wall(User wallUser) throws UserNotFoundException;

    boolean delete(User usr) throws UserNotFoundException;
}
