package com.epam.test.messenger.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private List<Message> messages = new ArrayList<Message>();
    private List<User> followed = new ArrayList<User>();

    private User(String name) {
        this.name = name;
    }

    public static User newInstance(String name) {
        return new User(name);
    }

    public boolean alreadyFollowed(User followed) {
        for (User usr : getFollowed()) {
            if (usr.getName().equals(followed.getName())) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<User> getFollowed() {
        return followed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Message m : this.getMessages()) {
            sb.append(m.toString());
        }
        return sb.toString();
    }
}
