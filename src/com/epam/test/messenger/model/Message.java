package com.epam.test.messenger.model;

import java.util.Date;

public class Message {

    private String messageData;
    private Date timestamp;

    private Message(String messageData) {
        this.messageData = messageData;
    }

    public static Message newInstance(String testMessage) {
        return new Message(testMessage);
    }

    public String getMessageData() {
        return messageData;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return this.getMessageData() + " (" + printTimeElapsed() + ")\n";
    }

    public String printTimeElapsed() {
        Date currentDate = new Date();
        int time = Math.round(((currentDate.getTime() - this.getTimestamp().getTime()) / 1000F));
        String stime = String.valueOf(time);
        boolean plural = time == 11 || (stime.charAt(stime.length() - 1)) != '1';
        if (time < 60) {
            return "" + time + (plural ? " seconds" : " second") + " ago";
        } else {
            time = time / 60;
            return "" + time + (plural ? " minutes" : " minute") + " ago";
        }
    }
}
