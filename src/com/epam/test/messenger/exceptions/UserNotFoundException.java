package com.epam.test.messenger.exceptions;

public class UserNotFoundException extends Exception {
    private final String userName;

    public UserNotFoundException(String msg, String userName) {
        super(msg);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
