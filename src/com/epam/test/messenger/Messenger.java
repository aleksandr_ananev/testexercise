package com.epam.test.messenger;

import com.epam.test.messenger.dao.IMessengerDAO;
import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;

import java.util.List;

public class Messenger implements IMessenger {

    private IMessengerDAO dao;

    private Messenger(final IMessengerDAO dao) {
        this.dao = dao;
    }

    public static IMessenger newInstance(final IMessengerDAO dao) {
        return new Messenger(dao);
    }

    @Override
    public boolean post(final User usr, final Message msg) throws UserNotFoundException {
        if (usr == null) {
            throw new UserNotFoundException("Cannot call post with Null user", "null");
        }
        if (msg == null) {
            throw new UserNotFoundException("Cannot call post with Null message", "null");
        }
        return dao.post(usr, msg);
    }

    @Override
    public List<Message> read(final User usr) throws UserNotFoundException {
        if (usr == null) {
            throw new UserNotFoundException("Cannot call post with Null user", "null");
        }
        return dao.read(usr);
    }

    @Override
    public void follow(final User follower, final User followed) throws UserNotFoundException {
        if (follower == null) {
            throw new UserNotFoundException("Cannot call follows with Null follower user", "null");
        }
        if (followed == null) {
            throw new UserNotFoundException("Cannot call follows with Null followed user", "null");
        }
        dao.follow(follower, followed);
    }

    @Override
    public List<Message> wall(final User wallUser) throws UserNotFoundException {
        if (wallUser == null) {
            throw new UserNotFoundException("Cannot call wall with Null user", "null");
        }
        return dao.wall(wallUser);
    }

    @Override
    public boolean delete(final User usr) throws UserNotFoundException {
        if (usr == null) {
            throw new UserNotFoundException("Cannot call wall with Null user", "null");
        }
        return dao.deleteLastMessage(usr);
    }

}
