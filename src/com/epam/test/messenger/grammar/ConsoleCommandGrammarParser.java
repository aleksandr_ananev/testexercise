// Generated from D:\work\antlrtest\ConsoleCommandGrammar.g4 by ANTLR 4.1
package com.epam.test.messenger.grammar;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNSimulator;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ConsoleCommandGrammarParser extends Parser {
    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    public static final int
            T__3 = 1, T__2 = 2, T__1 = 3, T__0 = 4, TEXT = 5, WS = 6;
    public static final String[] tokenNames = {
            "<INVALID>", "'follows'", "'->'", "'delete'", "'wall'", "TEXT", "WS"
    };
    public static final int
            RULE_command = 0, RULE_leftpart = 1, RULE_rightpart = 2, RULE_unaryOp = 3,
            RULE_binaryOp = 4;
    public static final String[] ruleNames = {
            "command", "leftpart", "rightpart", "unaryOp", "binaryOp"
    };

    @Override
    public String getGrammarFileName() {
        return "ConsoleCommandGrammar.g4";
    }

    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public ConsoleCommandGrammarParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class CommandContext extends ParserRuleContext {
        public UnaryOpContext unaryOp() {
            return getRuleContext(UnaryOpContext.class, 0);
        }

        public RightpartContext rightpart() {
            return getRuleContext(RightpartContext.class, 0);
        }

        public BinaryOpContext binaryOp() {
            return getRuleContext(BinaryOpContext.class, 0);
        }

        public LeftpartContext leftpart() {
            return getRuleContext(LeftpartContext.class, 0);
        }

        public CommandContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_command;
        }
    }

    public final CommandContext command() throws RecognitionException {
        CommandContext _localctx = new CommandContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_command);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(10);
                leftpart();
                setState(17);
                switch (getInterpreter().adaptivePredict(_input, 1, _ctx)) {
                    case 1: {
                        setState(12);
                        switch (getInterpreter().adaptivePredict(_input, 0, _ctx)) {
                            case 1: {
                                setState(11);
                                unaryOp();
                            }
                            break;
                        }
                    }
                    break;

                    case 2: {
                        setState(14);
                        binaryOp();
                        setState(15);
                        rightpart();
                    }
                    break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LeftpartContext extends ParserRuleContext {
        public TerminalNode TEXT(int i) {
            return getToken(ConsoleCommandGrammarParser.TEXT, i);
        }

        public List<TerminalNode> TEXT() {
            return getTokens(ConsoleCommandGrammarParser.TEXT);
        }

        public LeftpartContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_leftpart;
        }
    }

    public final LeftpartContext leftpart() throws RecognitionException {
        LeftpartContext _localctx = new LeftpartContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_leftpart);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(20);
                switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
                    case 1: {
                        setState(19);
                        _la = _input.LA(1);
                        if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 3) | (1L << 4))) != 0))) {
                            _errHandler.recoverInline(this);
                        }
                        consume();
                    }
                    break;
                }
                setState(25);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 3, _ctx);
                while (_alt != 2 && _alt != -1) {
                    if (_alt == 1) {
                        {
                            {
                                setState(22);
                                match(TEXT);
                            }
                        }
                    }
                    setState(27);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 3, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RightpartContext extends ParserRuleContext {
        public TerminalNode TEXT(int i) {
            return getToken(ConsoleCommandGrammarParser.TEXT, i);
        }

        public List<TerminalNode> TEXT() {
            return getTokens(ConsoleCommandGrammarParser.TEXT);
        }

        public RightpartContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_rightpart;
        }
    }

    public final RightpartContext rightpart() throws RecognitionException {
        RightpartContext _localctx = new RightpartContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_rightpart);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(29);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 4, _ctx);
                do {
                    switch (_alt) {
                        case 1: {
                            {
                                setState(28);
                                _la = _input.LA(1);
                                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 3) | (1L << 4) | (1L << TEXT))) != 0))) {
                                    _errHandler.recoverInline(this);
                                }
                                consume();
                            }
                        }
                        break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(31);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 4, _ctx);
                } while (_alt != 2 && _alt != -1);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class UnaryOpContext extends ParserRuleContext {
        public UnaryOpContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unaryOp;
        }
    }

    public final UnaryOpContext unaryOp() throws RecognitionException {
        UnaryOpContext _localctx = new UnaryOpContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_unaryOp);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(33);
                _la = _input.LA(1);
                if (!(_la == 3 || _la == 4)) {
                    _errHandler.recoverInline(this);
                }
                consume();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BinaryOpContext extends ParserRuleContext {
        public BinaryOpContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_binaryOp;
        }
    }

    public final BinaryOpContext binaryOp() throws RecognitionException {
        BinaryOpContext _localctx = new BinaryOpContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_binaryOp);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(35);
                _la = _input.LA(1);
                if (!(_la == 1 || _la == 2)) {
                    _errHandler.recoverInline(this);
                }
                consume();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN =
            "\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\b(\4\2\t\2\4\3\t" +
                    "\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\5\2\17\n\2\3\2\3\2\3\2\5\2\24\n\2\3" +
                    "\3\5\3\27\n\3\3\3\7\3\32\n\3\f\3\16\3\35\13\3\3\4\6\4 \n\4\r\4\16\4!\3" +
                    "\5\3\5\3\6\3\6\3\6\2\7\2\4\6\b\n\2\6\3\2\3\6\3\2\3\7\3\2\5\6\3\2\3\4(" +
                    "\2\f\3\2\2\2\4\26\3\2\2\2\6\37\3\2\2\2\b#\3\2\2\2\n%\3\2\2\2\f\23\5\4" +
                    "\3\2\r\17\5\b\5\2\16\r\3\2\2\2\16\17\3\2\2\2\17\24\3\2\2\2\20\21\5\n\6" +
                    "\2\21\22\5\6\4\2\22\24\3\2\2\2\23\16\3\2\2\2\23\20\3\2\2\2\23\24\3\2\2" +
                    "\2\24\3\3\2\2\2\25\27\t\2\2\2\26\25\3\2\2\2\26\27\3\2\2\2\27\33\3\2\2" +
                    "\2\30\32\7\7\2\2\31\30\3\2\2\2\32\35\3\2\2\2\33\31\3\2\2\2\33\34\3\2\2" +
                    "\2\34\5\3\2\2\2\35\33\3\2\2\2\36 \t\3\2\2\37\36\3\2\2\2 !\3\2\2\2!\37" +
                    "\3\2\2\2!\"\3\2\2\2\"\7\3\2\2\2#$\t\4\2\2$\t\3\2\2\2%&\t\5\2\2&\13\3\2" +
                    "\2\2\7\16\23\26\33!";
    public static final ATN _ATN =
            ATNSimulator.deserialize(_serializedATN.toCharArray());

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}