grammar ConsoleCommandGrammar;
command        : leftpart (unaryOp?|binaryOp rightpart)?;
leftpart       : ('wall'|'delete'|'follows'|'->')? TEXT*;
rightpart      : (TEXT | 'wall'|'delete'|'follows'|'->')+;
unaryOp        : 'wall'|'delete';
binaryOp       : ('->'|'follows');
TEXT           : ~[ \r\n]+;
WS             : [ \t\r\n] -> skip;