package com.epam.test.messenger.helpers;

import com.epam.test.messenger.grammar.ConsoleCommandGrammarLexer;
import com.epam.test.messenger.grammar.ConsoleCommandGrammarParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class GrammarCommandParser implements ICommandParser {
    private final ConsoleCommandGrammarLexer glexer;
    private final ConsoleCommandGrammarParser gparser;

    private GrammarCommandParser() {
        glexer = new ConsoleCommandGrammarLexer(new ANTLRInputStream(""));
        gparser = new ConsoleCommandGrammarParser(new CommonTokenStream(glexer));
    }

    public static ICommandParser newInstance() {
        return new GrammarCommandParser();
    }

    @Override
    public ICommand parseCommand(String input) {
        glexer.setInputStream(new ANTLRInputStream(input));
        gparser.setTokenStream(new CommonTokenStream(glexer));

        return parseCommand(gparser.command());
    }

    private ICommand parseCommand(ConsoleCommandGrammarParser.CommandContext ctx) {
        String leftPart = getLeftpart(ctx.leftpart());
        String unaryOp = getUnaryOp(ctx.unaryOp());
        String binaryOp = getBinaryOp(ctx.binaryOp());
        String rightPart = getRightpart(ctx.rightpart());

        String op;
        if (unaryOp != null) {
            op = unaryOp;
        } else {
            op = binaryOp;
        }
        return ConsoleCommand.newInstance(leftPart, op, rightPart);
    }

    private String getLeftpart(ConsoleCommandGrammarParser.LeftpartContext ctx) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ctx.getChildCount(); i++) {
            sb.append(ctx.getChild(i).getText());
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private String getRightpart(ConsoleCommandGrammarParser.RightpartContext ctx) {
        if (ctx == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ctx.getChildCount(); i++) {
            sb.append(ctx.getChild(i).getText());
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private String getUnaryOp(ConsoleCommandGrammarParser.UnaryOpContext ctx) {
        return ctx == null ? null : ctx.getText().trim();
    }

    private String getBinaryOp(ConsoleCommandGrammarParser.BinaryOpContext ctx) {
        return ctx == null ? null : ctx.getText().trim();
    }

}
