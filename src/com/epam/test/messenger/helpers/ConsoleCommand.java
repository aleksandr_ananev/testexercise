package com.epam.test.messenger.helpers;

import com.epam.test.messenger.IMessenger;
import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;

public class ConsoleCommand implements ICommand {
    private OperationEnum opEnum;
    private String leftValue;
    private String rightValue;

    private ConsoleCommand(String leftValue, String op, String rightValue) {
        this.leftValue = leftValue;
        if (op == null) {
            this.opEnum = OperationEnum.READ;
        } else {
            this.opEnum = OperationEnum.fromString(op);
        }
        this.rightValue = rightValue;
    }

    public static ICommand newInstance(String leftValue, String op, String rightValue) {
        return new ConsoleCommand(leftValue, op, rightValue);
    }

    public static enum OperationEnum {
        POST("->"),
        READ(""),
        FOLLOW("follows"),
        WALL("wall"),
        DELETE("delete");

        String value;

        OperationEnum(String value) {
            this.value = value;
        }

        public static OperationEnum fromString(String text) {
            if (text != null) {
                for (OperationEnum e : OperationEnum.values()) {
                    if (text.equalsIgnoreCase(e.value)) {
                        return e;
                    }
                }
            }
            return null;
        }
    }

    @Override
    public String execute(IMessenger messenger) {
        switch (opEnum) {
            case POST: {
                try {
                    messenger.post(User.newInstance(leftValue), Message.newInstance(rightValue));
                } catch (UserNotFoundException e) {
                    return e.getMessage();
                }
                return "";
            }
            case READ: {
                try {
                    StringBuilder sb = new StringBuilder();
                    for (Message m : messenger.read(User.newInstance(leftValue))) {
                        sb.append(m.toString());
                    }
                    return sb.toString();
                } catch (UserNotFoundException e) {
                    return e.getMessage();
                }
            }
            case FOLLOW: {
                try {
                    messenger.follow(User.newInstance(leftValue), User.newInstance(rightValue));
                } catch (UserNotFoundException e) {
                    return e.getMessage();
                }
                return "";
            }
            case WALL: {
                try {
                    StringBuilder sb = new StringBuilder();
                    for (Message m : messenger.wall(User.newInstance(leftValue))) {
                        sb.append(m.toString());
                    }
                    return sb.toString();
                } catch (UserNotFoundException e) {
                    return e.getMessage();
                }
            }
            case DELETE: {
                try {
                    messenger.delete(User.newInstance(leftValue));
                } catch (UserNotFoundException e) {
                    return e.getMessage();
                }
                return "";
            }
            default:
                return "Incorrect command!!";
        }
    }
}
