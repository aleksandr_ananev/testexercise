package com.epam.test.messenger.helpers;

public interface ICommandParser {
    public ICommand parseCommand(String input);
}
