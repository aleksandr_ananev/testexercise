package com.epam.test.messenger.helpers;

import com.epam.test.messenger.IMessenger;

public interface ICommand {
    String execute(IMessenger messenger);
}
