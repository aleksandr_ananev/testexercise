package com.epam.test.messenger;

import com.epam.test.messenger.dao.MessengerMapDao;
import com.epam.test.messenger.helpers.GrammarCommandParser;
import com.epam.test.messenger.helpers.ICommand;
import com.epam.test.messenger.helpers.ICommandParser;
import com.epam.test.messenger.model.User;

import java.io.*;
import java.util.HashMap;

public class ConsoleMessenger {
    private final IMessenger messenger;
    private final ICommandParser parser = GrammarCommandParser.newInstance();

    public ConsoleMessenger(IMessenger messenger) {
        this.messenger = messenger;
    }

    public static void main(String[] args) {
        ConsoleMessenger cmes = new ConsoleMessenger(Messenger.newInstance(MessengerMapDao.newInstance(new HashMap<String, User>())));
        cmes.run(System.in, System.out);
    }

    public void run(InputStream in, OutputStream out) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        PrintStream ps = new PrintStream(out);
        try {
            String input;
            while (true) {
                ps.print("> ");
                input = br.readLine();
                if (input != null && input.equals("quit")) {
                    break;
                }
                ps.println(parseCommand(input).execute(messenger));
            }
        } catch (IOException e) {
            ps.printf("Unexpected error occurred: " + e.getMessage());
        }
    }

    public ICommand parseCommand(String input) {
        return parser.parseCommand(input);
    }
}
