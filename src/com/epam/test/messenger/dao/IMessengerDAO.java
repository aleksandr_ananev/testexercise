package com.epam.test.messenger.dao;

import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;

import java.util.List;

public interface IMessengerDAO {
    boolean post(User usr, Message msg);

    List<Message> read(User usr) throws UserNotFoundException;

    void follow(User follower, User followed) throws UserNotFoundException;

    List<Message> wall(User usr) throws UserNotFoundException;

    boolean deleteLastMessage(User usr) throws UserNotFoundException;
}
