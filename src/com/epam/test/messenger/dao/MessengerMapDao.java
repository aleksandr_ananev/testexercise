package com.epam.test.messenger.dao;

import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;

import java.util.*;

public class MessengerMapDao implements IMessengerDAO {
    private final Map<String, User> users;

    private MessengerMapDao(Map<String, User> users) {
        this.users = users;
    }

    public static IMessengerDAO newInstance(Map<String, User> users) {
        return new MessengerMapDao(users);
    }

    public static IMessengerDAO newInstance() {
        return newInstance(new HashMap<String, User>());
    }

    @Override
    public boolean post(User usr, Message msg) {
        if (!exists(usr)) {
            users.put(usr.getName(), usr);
        }
        msg.setTimestamp(new Date());
        getUser(usr).getMessages().add(msg);
        return true;
    }

    @Override
    public List<Message> read(User usr) throws UserNotFoundException {
        User user = getUser(usr);
        if (user == null) {
            throw new UserNotFoundException("User " + usr.getName() + " is unknown. Register the user by posting a message first.", usr.getName());
        }
        return user.getMessages();
    }

    @Override
    public void follow(User follower, User followed) throws UserNotFoundException {
        if (follower.getName().equals(followed.getName())) {
            //Sorry, you can't follow yourself.
            return;
        }
        if (exists(followed)) {
            if (!exists(follower)) {
                throw new UserNotFoundException("User " + follower.getName() + " is unknown. Register the user by posting a message first.", follower.getName());
            }
            if (!getUser(follower).alreadyFollowed(followed)) {
                getUser(follower).getFollowed().add(getUser(followed));
            }
        } else {
            throw new UserNotFoundException("Found no user named " + followed.getName() + " to follow", followed.getName());
        }
    }

    @Override
    public List<Message> wall(User usr) throws UserNotFoundException {
        if (exists(usr)) {
            List<Message> result = new ArrayList<Message>();
            User requestUser = getUser(usr);
            prependMessagesWithUserName(result, requestUser);
            for (User followed : requestUser.getFollowed()) {
                prependMessagesWithUserName(result, followed);
            }
            //Sort messages on the wall. Newest on top.
            Collections.sort(result, new Comparator<Message>() {
                @Override
                public int compare(final Message o1, final Message o2) {
                    return o2.getTimestamp().compareTo(o1.getTimestamp());
                }
            });
            return result;
        }
        throw new UserNotFoundException("User " + usr.getName() + " is unknown. Register the user by posting a message first.", usr.getName());
    }

    @Override
    public boolean deleteLastMessage(User usr) throws UserNotFoundException {
        if (exists(usr)) {
            List<Message> msgs = getUser(usr).getMessages();
            if (msgs.size() > 0) {
                msgs.remove(msgs.size() - 1);
                return true;
            }
            return false;
        }
        throw new UserNotFoundException("User " + usr.getName() + " is unknown. Register the user by posting a message first.", usr.getName());
    }

    private void prependMessagesWithUserName(List<Message> result, User requestUser) {
        for (Message m : requestUser.getMessages()) {
            Message msg = Message.newInstance(requestUser.getName() + " - " + m.getMessageData());
            msg.setTimestamp(m.getTimestamp());
            result.add(msg);
        }
    }

    private boolean exists(User user) {
        return users.containsKey(user.getName());
    }

    private User getUser(User usr) {
        return users.get(usr.getName());
    }
}
