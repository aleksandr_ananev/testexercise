import com.epam.test.messenger.IMessenger;
import com.epam.test.messenger.Messenger;
import com.epam.test.messenger.dao.IMessengerDAO;
import com.epam.test.messenger.dao.MessengerMapDao;
import com.epam.test.messenger.exceptions.UserNotFoundException;
import com.epam.test.messenger.model.Message;
import com.epam.test.messenger.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MessengerTest {
    Map<String, User> mapMock;
    IMessengerDAO dao;
    IMessenger messenger;

    @Before
    public void init() {
        mapMock = spy(new HashMap<String, User>());
        dao = MessengerMapDao.newInstance(mapMock);
        messenger = Messenger.newInstance(dao);
    }

    @Test
    public void postMessageTest() throws UserNotFoundException {

        User usr = User.newInstance("TestUser");
        String testMessage = "Test message";
        Message msg = Message.newInstance(testMessage);

        messenger.post(usr, msg);

        verify(mapMock).put(usr.getName(), usr);
        assertEquals(mapMock.get(usr.getName()).getMessages().get(0).getMessageData(), testMessage);
    }

    @Test
    public void postMessageByExistingUserTest() throws UserNotFoundException {
        User usr = User.newInstance("TestUser");
        String testMessage = "Test message";
        Message msg = Message.newInstance(testMessage);
        usr.getMessages().add(msg);
        mapMock.put(usr.getName(), usr);

        String testMessage2 = "Test message2";
        Message msg2 = Message.newInstance(testMessage2);

        reset(mapMock);
        messenger.post(usr, msg2);
        verify(mapMock).get(usr.getName());

        assertEquals(mapMock.get(usr.getName()).getMessages().get(1).getMessageData(), testMessage2);
    }

    @Test
    public void readMessageTest() throws UserNotFoundException {
        User readUser = User.newInstance("ReadUser");
        readUser.getMessages().add(Message.newInstance("ReadTest"));

        mapMock.put(readUser.getName(), readUser);

        User usr = User.newInstance("ReadUser");
        List<Message> msgs = messenger.read(usr);
        verify(mapMock).get(usr.getName());

        assertEquals(msgs.get(0).getMessageData(), readUser.getMessages().get(0).getMessageData());
    }

    @Test(expected = UserNotFoundException.class)
    public void readMessageWhenNoSuchUserTest() throws UserNotFoundException {
        User readUser = User.newInstance("ReadUser");
        readUser.getMessages().add(Message.newInstance("ReadTest"));

        User usr = User.newInstance("ReadUser");
        messenger.read(usr);
    }

    @Test
    public void followUserTest() throws UserNotFoundException {
        User followerUser = User.newInstance("FollowerUser");
        followerUser.getMessages().add(Message.newInstance("FollowerTest"));
        mapMock.put(followerUser.getName(), followerUser);

        User followedUser = User.newInstance("FollowedUser");
        followedUser.getMessages().add(Message.newInstance("FollowedTest"));
        mapMock.put(followedUser.getName(), followedUser);

        User follower = spy(User.newInstance("FollowerUser"));
        User followed = User.newInstance("FollowedUser");
        messenger.follow(follower, followed);

        assertEquals(followerUser.getFollowed().get(0).getName(), followedUser.getName());
    }

    @Test(expected = UserNotFoundException.class)
    public void followUnknownUserTest() throws UserNotFoundException {
        User followerUser = User.newInstance("FollowerUser");
        followerUser.getMessages().add(Message.newInstance("FollowerTest"));
        mapMock.put(followerUser.getName(), followerUser);

        User followedUser = User.newInstance("FollowedUser");
        followedUser.getMessages().add(Message.newInstance("FollowedTest"));

        User follower = spy(User.newInstance("FollowerUser"));
        User followed = User.newInstance("FollowedUser");
        messenger.follow(follower, followed);
    }

    @Test(expected = UserNotFoundException.class)
    public void followByUnknownUserTest() throws UserNotFoundException {
        User followerUser = User.newInstance("FollowerUser");
        followerUser.getMessages().add(Message.newInstance("FollowerTest"));

        User followedUser = User.newInstance("FollowedUser");
        followedUser.getMessages().add(Message.newInstance("FollowedTest"));
        mapMock.put(followedUser.getName(), followedUser);

        User follower = spy(User.newInstance("FollowerUser"));
        User followed = User.newInstance("FollowedUser");
        messenger.follow(follower, followed);
    }

    @Test
    public void followAlreadyFollowedUserTest() throws UserNotFoundException {
        User followerUser = User.newInstance("FollowerUser");
        followerUser.getMessages().add(Message.newInstance("FollowerTest"));
        mapMock.put(followerUser.getName(), followerUser);

        User followedUser = User.newInstance("FollowedUser");
        followedUser.getMessages().add(Message.newInstance("FollowedTest"));
        mapMock.put(followedUser.getName(), followedUser);

        followerUser.getFollowed().add(followedUser);

        User follower = spy(User.newInstance("FollowerUser"));
        User followed = User.newInstance("FollowedUser");

        messenger.follow(follower, followed);

        assertEquals(1, mapMock.get(followerUser.getName()).getFollowed().size());
    }

    @Test
    public void wallTest() throws UserNotFoundException {
        User wallUser = User.newInstance("WallUser");
        wallUser.getMessages().add(Message.newInstance("WallEntry"));
        mapMock.put(wallUser.getName(), wallUser);

        List<Message> wallMessages;
        wallMessages = messenger.wall(wallUser);


        assertEquals("WallUser - WallEntry", wallMessages.get(0).getMessageData());
    }

    @Test(expected = UserNotFoundException.class)
    public void postByNullUserTest() throws UserNotFoundException {
        messenger.post(null, Message.newInstance("test"));
    }

    @Test(expected = UserNotFoundException.class)
    public void postNullMessageTest() throws UserNotFoundException {
        messenger.post(User.newInstance("Test"), null);
    }

    @Test(expected = UserNotFoundException.class)
    public void readNullUserTest() throws UserNotFoundException {
        messenger.read(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void followByNullUserTest() throws UserNotFoundException {
        messenger.follow(null, User.newInstance("Followed"));
    }

    @Test(expected = UserNotFoundException.class)
    public void followNullUserTest() throws UserNotFoundException {
        messenger.follow(User.newInstance("Follower"), null);
    }

    @Test(expected = UserNotFoundException.class)
    public void wallNullUserTest() throws UserNotFoundException {
        messenger.wall(null);
    }


}
