import com.epam.test.messenger.ConsoleMessenger;
import com.epam.test.messenger.IMessenger;
import com.epam.test.messenger.Messenger;
import com.epam.test.messenger.dao.MessengerMapDao;
import com.epam.test.messenger.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

public class ConsoleMessengerTest {
    ConsoleMessenger console;
    IMessenger messengerSpy;

    @Before
    public void init() {
        messengerSpy = spy(Messenger.newInstance(MessengerMapDao.newInstance(new HashMap<String, User>())));

        console = new ConsoleMessenger(messengerSpy);
    }

    @Test
    public void postMessageNewUserTest() {
        assertEquals("", console.parseCommand("Alice -> I love the weather today").execute(messengerSpy));

    }

    @Test
    public void postTwoMessagesSameUserTest() {
        console.parseCommand("Bob -> Oh, we lost!").execute(messengerSpy);
        assertEquals("", console.parseCommand("Bob -> at least it's sunny").execute(messengerSpy));
    }

    @Test
    public void readPreviouslyPostedSingleMessageSameUserTest() {
        console.parseCommand("Alice -> I love the weather today").execute(messengerSpy);
        assertEquals("I love the weather today (0 seconds ago)\n", console.parseCommand("Alice").execute(messengerSpy));
    }

    @Test
    public void readPreviouslyPostedSeveralMessagesSameUserTest() {
        console.parseCommand("Bob -> Oh, we lost!").execute(messengerSpy);
        console.parseCommand("Bob -> at least it's sunny").execute(messengerSpy);
        assertEquals("Oh, we lost! (0 seconds ago)\n" +
                "at least it's sunny (0 seconds ago)\n", console.parseCommand("Bob").execute(messengerSpy));
    }

    @Test
    public void existingUserFollowAndWallTest() throws InterruptedException {
        console.parseCommand("Alice -> I love the weather today").execute(messengerSpy);
        Thread.sleep(10);
        console.parseCommand("Bob -> Oh, we lost!").execute(messengerSpy);
        Thread.sleep(10);
        console.parseCommand("Bob -> at least it's sunny").execute(messengerSpy);
        Thread.sleep(10);
        console.parseCommand("Charlie -> I'm in New York today! Anyone wants to have a coffee?").execute(messengerSpy);
        Thread.sleep(10);
        console.parseCommand("Charlie follows Alice").execute(messengerSpy);

        assertEquals("Charlie - I'm in New York today! Anyone wants to have a coffee? (0 seconds ago)\n" +
                "Alice - I love the weather today (0 seconds ago)\n", console.parseCommand("Charlie wall").execute(messengerSpy));

        console.parseCommand("Charlie follows Bob").execute(messengerSpy);

        assertEquals("Charlie - I'm in New York today! Anyone wants to have a coffee? (0 seconds ago)\n" +
                "Bob - at least it's sunny (0 seconds ago)\n" +
                "Bob - Oh, we lost! (0 seconds ago)\n" +
                "Alice - I love the weather today (0 seconds ago)\n", console.parseCommand("Charlie wall").execute(messengerSpy));
    }

    @Test
    public void unexistingUserFollowAndWallTest() throws InterruptedException {
        assertEquals("User Alice is unknown. Register the user by posting a message first.", console.parseCommand("Alice wall").execute(messengerSpy));
    }
}
